package message;

import java.io.Serializable;

/**
 * Created by Руслан on 01.12.2016.
 */
public class Message implements Serializable, Cloneable{

    protected String data;
    protected long date;

    public Message(String data) {
        this.data = data;
        date = System.currentTimeMillis();
    }
    public Message(Message message){
        this.data=message.getData();
        this.date=message.getDate();
    }

    public String getData() {
        return data;
    }

    public long getDate() {
        return date;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public Message clone() throws CloneNotSupportedException {
        return (Message)super.clone();
    }
}
