package server.recognizedmessage;

import message.Message;

/**
 * Created by Руслан on 01.12.2016.
 */
public class RecognizedMessage extends Message {
    private MessageType messageType;


    private String answer;
    public MessageType getMessageType() {
        return messageType;
    }

    public RecognizedMessage(Message message, MessageType messageType) {
        super(message);
        this.messageType = messageType;
    }
    public void setData(String data){
        this.data=data;
    }
    public void setWrongCommandType(){
        messageType = MessageType.wrongCommand;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

}
