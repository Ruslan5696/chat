package server.recognizedmessage;

/**
 * Created by Руслан on 01.12.2016.
 */
public enum MessageType {
    text(true),
    reg(false),
    login(false),
    exit(false),
    wrongCommand(true),
    prvt(true);
    private boolean authenticationRequired;

    MessageType(boolean authenticationRequired) {
        this.authenticationRequired = authenticationRequired;
    }

    public boolean isAuthenticationRequired() {
        return authenticationRequired;
    }
}
