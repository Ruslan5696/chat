package server.authentication;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by Руслан on 06.12.2016.
 */
public class Authentication {
    private static final String NAMES_PATH = ".\\src\\server\\authentication\\names";
    private static final String SALT_PATH = ".\\src\\server\\authentication\\salt";
    private static final String HASH_PATH = ".\\src\\server\\authentication\\hash";

    public static final String NAME_BUSY = "Имя занято";
    public static final String PASSWORDS_NOT_SAME = "Пароли не совпадают";
    public static final String SUCCESS = "Успешно";


    public static void main(String[] args) throws Exception {
        readLogOrReg();

    }

    public static void readLogOrReg() throws Exception {
        String inFile = "";
        FileReader reader = new FileReader("C:\\Users\\Руслан\\workspace\\Chat\\src\\server\\authentication\\file");
        while (reader.ready())
            inFile += (char) reader.read();
        String[] parsed = inFile.split(System.lineSeparator());

        if (parsed.length == 2) {
            System.out.println(login(parsed));
        } else if ((parsed.length == 3))
            registration(parsed);
        else
            System.out.println("error");
    }

    private static int getUserNumber(String name) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(NAMES_PATH));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int count = 0;
        while (scanner.hasNextLine()) {
            String nextLine = scanner.nextLine();
            if (name.equals(nextLine))
                return count;
            else count++;
        }
        return -1;
    }


    private static long getSalt(String name) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File(SALT_PATH));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int count = getUserNumber(name);
        if (count == -1)
            throw new RuntimeException(name + " not exist");
        else
            for (int i = 0; i < count; i++) {
                scanner.nextLine();
            }
        return Long.valueOf(scanner.nextLine());
    }


    private static byte[] getHash(String name) {
        byte[] hash = new byte[32];
        InputStream inputStream;
        try {
            inputStream = new FileInputStream(HASH_PATH);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Файл " + HASH_PATH + " не найден");

        }
        int count = getUserNumber(name);
        if (count == -1)
            throw new RuntimeException(name + " not exist");
        else
            try {
                for (int i = 0; i < count; i++) {
                    inputStream.read(hash);
                }
                inputStream.read(hash);
            } catch (IOException e) {
                e.printStackTrace();
            }
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hash;
    }

    public static boolean login(String[] data) {
        if (getUserNumber(data[0]) == -1)
            return false;
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(((data[1] + getSalt(data[0]))).getBytes());

        byte[] sByte = md.digest();
        return Arrays.equals(sByte, getHash(data[0]));
    }

    public static String registration(String[] data) {
        if (getUserNumber(data[0]) != -1) {
            return NAME_BUSY;
        }
        if (!data[1].equals(data[2])) {
            return PASSWORDS_NOT_SAME;
        }

        FileOutputStream namesOutputStream = null;
        FileOutputStream hashOutputStream = null;
        FileOutputStream saltOutputStream = null;
        try {
            namesOutputStream = new FileOutputStream(NAMES_PATH, true);
            hashOutputStream = new FileOutputStream(HASH_PATH, true);
            saltOutputStream = new FileOutputStream(SALT_PATH, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        String salt = String.valueOf(new Date().getTime());
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update((data[1] + salt).getBytes()); // Change this to "UTF-16" if needed
        byte[] hash = md.digest();
        try {
            hashOutputStream.write(hash);
            namesOutputStream.write((data[0] + '\n').getBytes());
            saltOutputStream.write((salt + '\n').getBytes());
            namesOutputStream.close();
            hashOutputStream.close();
            saltOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SUCCESS;
    }
}
