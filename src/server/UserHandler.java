package server;

import message.Message;
import server.messagehandler.AbstractMessageHandler;
import server.messagehandler.MessageHandlerFactory;
import server.quarantine.Quarantine;
import server.recognizedmessage.RecognizedMessage;

import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Руслан on 06.12.2016.
 */
public class UserHandler  {
    protected ConcurrentLinkedQueue<Message> dirtyMessages = new ConcurrentLinkedQueue<>();
    protected ConcurrentLinkedQueue<RecognizedMessage> clearMessages = new ConcurrentLinkedQueue<>();

    protected User user;
    protected Quarantine quarantine;
    private MessageReceiverThread messageReceiver;
    private MessageHandlerThread messageHandler;


    public UserHandler(Socket socket) {
        user = new User(socket);
        user.keyExchange();
        quarantine = new Quarantine(dirtyMessages, clearMessages);
        quarantine.start();
        messageReceiver = new MessageReceiverThread();
        messageReceiver.start();
        messageHandler = new MessageHandlerThread();
        messageHandler.start();
        //user.sendMessage(new Message(Server.COMMANDS));
    }

    class MessageHandlerThread extends Thread {
        @Override
        public void run() {
            waitMessageHandlerThread();
            while (user.isConnected()) {
                if (!clearMessages.isEmpty()) {
                    AbstractMessageHandler messageHandler;
                    messageHandler = MessageHandlerFactory.getMessageHandler(clearMessages.peek().getMessageType());
                    messageHandler.handle(user, clearMessages.poll());
                } else {
                    waitMessageHandlerThread();
                }
            }
        }


        private void waitMessageHandlerThread(){
            synchronized (clearMessages) {
                try {
                    clearMessages.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class MessageReceiverThread extends Thread {
        @Override
        public void run() {

            while (user.isConnected()) {
                dirtyMessages.add(user.receiveMessage());
                synchronized (quarantine) {
                    quarantine.notify();
                }
            }
        }
    }
}