package server;

import consolehelper.ConsoleHelper;
import message.Message;
import server.quarantine.Quarantine;
import server.recognizedmessage.RecognizedMessage;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

/**
 * Created by Руслан on 01.12.2016.
 */
public class Server {
    private static Set<User> users = new CopyOnWriteArraySet<>();
    public static final String USER_DONT_EXIST = "Server: Пользователь не существует";
    public static final String NOT_AUTHORIZED = "Server: Войдите или зарегестрируйтесь в чате";
    public static final String ALREADY_AUTHORIZED = "Server: Вы уже авторизованы";
    public static final String WRONG_LOGIN_OR_PASSWORD = "Неверынй логин или пароль";
    public static final String SUCCESS = "Успешно";
    public static final String COMMANDS =
            "-reg userName password password\n" +
                    "-login login password\n" +
                    "-exit\n" +
                    "-prvt userName \"message\"";

    public static void main(String[] args) {
        System.out.println("Введите нормер порта");
        try (ServerSocket serverSocket = new ServerSocket(ConsoleHelper.readInt())) {
            while (true)
                new UserHandler(serverSocket.accept());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        Server.UserHandler userHandler = new UserHandler();
//        userHandler.start();
//        try {
//            Thread.sleep(500);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        Server.UserHandler userHandler2 = new UserHandler();
//        userHandler2.start();
    }

    public static void addUser(User user) {
        sendBroadcastMessage(new Message("Пользователь " + user.getName() + " вошел в чат"));
        users.add(user);
    }

    public static void remove(User user) {
        sendBroadcastMessage(new Message("Пользователь " + user.getName() + " вышел из чата"));
        users.remove(user);
    }

    public static void sendBroadcastMessage(Message message) {
        for (User user : users) {
            user.sendMessage(message);
        }
    }

    public static User getUser(String userName) {
        for (User user : users)
            if (userName.equals(user.getName()))
                return user;
        return null;
    }

    private static CopyOnWriteArraySet<User> clients = new CopyOnWriteArraySet<>();


}
