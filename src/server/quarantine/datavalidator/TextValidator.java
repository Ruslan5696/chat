package server.quarantine.datavalidator;

import server.recognizedmessage.MessageType;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 02.12.2016.
 */
public class TextValidator extends AbstractValidator {
    @Override
    public RecognizedMessage getValidMessage(RecognizedMessage message) {
        if (message.getData().matches("(К|к)урсов(ая|ой|ую)")){
            message.setWrongCommandType();
            message.setAnswer("Курсовая не должна упоминаться");
        }
        return message;
    }
}
