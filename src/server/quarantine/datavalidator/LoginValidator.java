package server.quarantine.datavalidator;

import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 02.12.2016.
 */
public class LoginValidator extends AbstractValidator {
    @Override
    public RecognizedMessage getValidMessage(RecognizedMessage message) {
        String[] data  = message.getData().split(" ");
        if (data.length > 3){
            message.setWrongCommandType();
            message.setAnswer("Не должно быть никаких символов, кроме логина и пароля");

        }
        return message;
    }
}
