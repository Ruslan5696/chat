package server.quarantine.datavalidator;

import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 02.12.2016.
 */
public class PrvtValidator extends AbstractValidator {
    @Override
    public RecognizedMessage getValidMessage(RecognizedMessage message) {
        String data = message.getData().substring(6);
        if (!data.matches("^\\w+\\s[\"].+[\"]$")) {
            System.out.println(data);
            message.setWrongCommandType();
            message.setAnswer("Неверный формат команды отправки личного сообщения (Формат: -prvt userName(a-Z_0-9) \"message\"");
        }
        return message;


    }
}
