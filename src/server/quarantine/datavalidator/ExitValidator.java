package server.quarantine.datavalidator;

import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 02.12.2016.
 */
public class ExitValidator extends AbstractValidator {
    @Override
    public RecognizedMessage getValidMessage(RecognizedMessage message) {
        if (message.getData().split(" ").length>1){
            message.setWrongCommandType();
            message.setAnswer("После команды -exit не должно быть никаких символов");
        }
        return message;
    }
}
