package server.quarantine.datavalidator;

import message.Message;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 02.12.2016.
 */
public abstract class AbstractValidator {

    public abstract RecognizedMessage getValidMessage(RecognizedMessage message);
}
