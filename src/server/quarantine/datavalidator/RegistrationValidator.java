package server.quarantine.datavalidator;

import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 02.12.2016.
 */
public class RegistrationValidator extends AbstractValidator {
    @Override
    public RecognizedMessage getValidMessage(RecognizedMessage message) {
        String[] data  = message.getData().split(" ");
        if(data.length!=4){
            message.setWrongCommandType();
            message.setAnswer("Не должно быть никаких символов, кроме логина и двух повторений пароля");
            return message;
        }
        if (!validLogin(data[1])){
            message.setWrongCommandType();
            message.setAnswer("Неверный формат логина (\\w{4,10}$)");
            return message;
        }
        if (!validPassword(data[2])){
            message.setWrongCommandType();
            message.setAnswer("Неверный формат пароля (\\w{4,10}$)");
            return message;
        }
        if (!data[2].equals(data[3])){
            message.setWrongCommandType();
            message.setAnswer("Пароли не совпадают");
                        return message;
        }
        return message;
    }

    private boolean validPassword(String password) {
        return password.matches("^\\w{4,10}$");
    }


    private boolean validLogin(String login) {
        return login.matches("^\\w{4,10}$");
    }
}
