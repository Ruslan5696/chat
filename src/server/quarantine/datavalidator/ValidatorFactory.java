package server.quarantine.datavalidator;

import server.recognizedmessage.MessageType;

/**
 * Created by Руслан on 02.12.2016.
 */
public class ValidatorFactory {
    public static AbstractValidator getValidator(MessageType messageType){
        switch (messageType){
            case login:
                return new LoginValidator();
            case reg:
                return new RegistrationValidator();
            case text:
                return new TextValidator();
            case prvt:
                return new PrvtValidator();
            case exit:
                return new ExitValidator();
            default:
                throw new RuntimeException("Unsupported message type ");
        }
    }
}
