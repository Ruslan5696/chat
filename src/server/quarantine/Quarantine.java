package server.quarantine;

import message.Message;
import server.quarantine.datavalidator.AbstractValidator;
import server.quarantine.datavalidator.ValidatorFactory;
import server.recognizedmessage.MessageType;
import server.recognizedmessage.RecognizedMessage;

import javax.xml.validation.Validator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Руслан on 02.12.2016.
 */
public class Quarantine extends Thread {
    private ConcurrentLinkedQueue<Message> dirtyMessages;
    private ConcurrentLinkedQueue<RecognizedMessage> recognizedMessagesWithDirtyData;
    private ConcurrentLinkedQueue<RecognizedMessage> clearMessages;
    private TypeRecognizer typeRecognizer = new TypeRecognizer();
    private DataValidator dataValidator = new DataValidator();

    public Quarantine(ConcurrentLinkedQueue<Message> dirtyMessage, ConcurrentLinkedQueue<RecognizedMessage> clearMessage) {
        this.dirtyMessages = dirtyMessage;
        this.clearMessages = clearMessage;
        recognizedMessagesWithDirtyData = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void run() {

        typeRecognizer.start();
        dataValidator.start();

        while (true) {
            notifyTypeRecognizerThread();
            //System.out.println("TypeRecognizer notify");
            waitQuarantineThread();
        }
    }


    private void waitQuarantineThread() {
        synchronized (this) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void notifyTypeRecognizerThread() {
        synchronized (typeRecognizer) {
            typeRecognizer.notify();
        }
    }

    private void waitTypeRecognizerThread() {
        synchronized (typeRecognizer) {
            try {
                typeRecognizer.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void waitDataValidatorThread() {
        synchronized (dataValidator) {
            try {
                dataValidator.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void notifyDataValidatorThread() {
        synchronized (dataValidator) {
            dataValidator.notify();
        }
    }

    class DataValidator extends Thread {
        @Override
        public void run() {
            waitDataValidatorThread();
            while (true) {
                if (!recognizedMessagesWithDirtyData.isEmpty()) {
                    AbstractValidator validator = ValidatorFactory.getValidator(recognizedMessagesWithDirtyData.peek().getMessageType());
                    RecognizedMessage clearMessage = validator.getValidMessage(recognizedMessagesWithDirtyData.poll());
                    clearMessages.add(clearMessage);
                    synchronized (clearMessages) {
                        clearMessages.notify();
                    }
                } else {
                    waitDataValidatorThread();
                }
            }
        }
    }

    class TypeRecognizer extends Thread {
        @Override
        public void run() {
            waitTypeRecognizerThread();
            while (true) {
                if (!dirtyMessages.isEmpty()) {
                    recognizeMessageType(dirtyMessages.poll());
                } else {
                    waitTypeRecognizerThread();
                }
            }

        }

        private boolean isCommand(Message message) {
            if (message.getData()!=null&& !message.getData().equals(""))
                return message.getData().split(" ")[0].charAt(0) == '-';
            return false;
        }

        private String getCommand(Message message) {
            String command = message.getData().split(" ")[0];
            return command;
        }

        private void recognizeMessageType(Message message) {
            if (!isCommand(message)) {
                recognizedMessagesWithDirtyData.add(new RecognizedMessage(message, MessageType.text));
                notifyDataValidatorThread();
            } else {
                switch (getCommand(message)) {
                    case "-login":
                        recognizedMessagesWithDirtyData.add(new RecognizedMessage(message, MessageType.login));
                        notifyDataValidatorThread();
                        break;
                    case "-reg":
                        recognizedMessagesWithDirtyData.add(new RecognizedMessage(message, MessageType.reg));
                        notifyDataValidatorThread();
                        break;
                    case "-prvt":
                        recognizedMessagesWithDirtyData.add(new RecognizedMessage(message, MessageType.prvt));
                        notifyDataValidatorThread();
                        break;
                    case "-exit":
                        recognizedMessagesWithDirtyData.add(new RecognizedMessage(message, MessageType.exit));
                        notifyDataValidatorThread();
                        break;
                    default:
                        RecognizedMessage clearMessage = new RecognizedMessage(message, MessageType.wrongCommand);
                        clearMessage.setAnswer("Комманада " + getCommand(clearMessage) + " не существует");
                        clearMessages.add(clearMessage);

                }
            }

        }
    }


}
