package server.messagehandler;

import message.Message;
import server.Server;
import server.User;
import server.authentication.Authentication;
import server.recognizedmessage.RecognizedMessage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Date;

/**
 * Created by Руслан on 06.12.2016.
 */
public class RegistrationMessageHandler extends AbstractMessageHandler {
    private final String PATH = ".\\src\\server\\names";
    @Override
    public void handle(User user, RecognizedMessage message) {
        if (user.isAuthorized()) {
            user.sendMessage(new Message(Server.ALREADY_AUTHORIZED));
            return;
        }
        String[] registrationData = Arrays.copyOfRange(message.getData().split(" "), 1, 4);
        String answer = Authentication.registration(registrationData);
        user.sendMessage(new Message("Server: "+answer));
        if (answer.equals(Authentication.SUCCESS)){
            user.setAuthorized(true);
            user.setName(registrationData[0]);
            Server.addUser(user);
        }
    }

}
