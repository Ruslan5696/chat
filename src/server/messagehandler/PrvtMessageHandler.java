package server.messagehandler;

import message.Message;
import server.Server;
import server.User;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 06.12.2016.
 */
public class PrvtMessageHandler extends AbstractMessageHandler {
    @Override
    public void handle(User user, RecognizedMessage message) {
        if (user.isAuthorized()){
            User recipient = Server.getUser(message.getData().split(" ")[1]);
            if (recipient!=null)
                recipient.sendMessage(new Message("Private message from "+user.getName()+": "+message.getData().split("\"")[1]));
            else
                user.sendMessage(new Message(Server.USER_DONT_EXIST));
        }
    }
}
