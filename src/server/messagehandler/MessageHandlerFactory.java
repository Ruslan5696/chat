package server.messagehandler;

import server.quarantine.datavalidator.*;
import server.recognizedmessage.MessageType;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 06.12.2016.
 */
public class MessageHandlerFactory {
    public static AbstractMessageHandler getMessageHandler(MessageType messageType){
        switch (messageType){
            case login:
                return new LoginMessageHandler();
            case reg:
                return new RegistrationMessageHandler();
            case text:
                return new TextMessageHandler();
            case prvt:
                return new PrvtMessageHandler();
            case exit:
                return new ExitMessageHandler();
            case wrongCommand:
                return new WrongMessageHandler();
            default:
                System.out.println(messageType);
                throw new RuntimeException("Unsupported message type ");
        }
    }
}
