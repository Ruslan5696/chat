package server.messagehandler;

import message.Message;
import server.User;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 07.12.2016.
 */
public class WrongMessageHandler extends AbstractMessageHandler{
    @Override
    public void handle(User user, RecognizedMessage message) {
        user.sendMessage(new Message(message.getAnswer()));
    }
}
