package server.messagehandler;

import message.Message;
import server.Server;
import server.User;
import server.authentication.Authentication;
import server.recognizedmessage.RecognizedMessage;

import java.util.Arrays;

/**
 * Created by Руслан on 06.12.2016.
 */
public class LoginMessageHandler extends AbstractMessageHandler {
    @Override
    public void handle(User user, RecognizedMessage message) {
        if (user.isAuthorized()) {
            user.sendMessage(new Message(Server.ALREADY_AUTHORIZED));
            return;
        }
        String[] loginData = Arrays.copyOfRange(message.getData().split(" "), 1, 3);
        Authentication.login(loginData);
        if (Authentication.login(loginData)){
            user.setAuthorized(true);
            user.setName(loginData[0]);
            Server.addUser(user);
            user.sendMessage(new Message(Server.SUCCESS));
        }else
            user.sendMessage(new Message(Server.WRONG_LOGIN_OR_PASSWORD));
    }
}
