package server.messagehandler;


import server.User;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 06.12.2016.
 */
public abstract class AbstractMessageHandler {
    public abstract void handle(User user, RecognizedMessage message);
}
