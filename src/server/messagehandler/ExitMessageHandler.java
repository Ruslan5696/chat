package server.messagehandler;

import message.Message;
import server.Server;
import server.User;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 06.12.2016.
 */
public class ExitMessageHandler extends AbstractMessageHandler {
    @Override
    public void handle(User user, RecognizedMessage message) {
        user.disconnect();
        if (user.isAuthorized()){
            Server.remove(user);
        }
    }
}
