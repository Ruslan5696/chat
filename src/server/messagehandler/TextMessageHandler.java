package server.messagehandler;

import message.Message;
import server.Server;
import server.User;
import server.recognizedmessage.RecognizedMessage;

/**
 * Created by Руслан on 06.12.2016.
 */
public class TextMessageHandler extends AbstractMessageHandler {
    @Override
    public void handle(User user, RecognizedMessage message) {
        if (user.isAuthorized())
            Server.sendBroadcastMessage(new Message(user.getName()+": "+message.getData()));
        else {
            user.sendMessage(new Message(Server.NOT_AUTHORIZED));
        }
    }
}
