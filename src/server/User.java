package server;

import diffieHellman.Encryption;
import message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by Руслан on 01.12.2016.
 */
public class User {
    Socket socket;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;
    private String name;
    private boolean authorized;
    private int encryptionKey;
    protected Encryption encryption;

    public User(Socket socket) {
        this.socket = socket;
        try {
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            encryption = new Encryption(outputStream, inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Message receiveMessage() {

        try {
            return encryption.encryptDecrypt((Message) inputStream.readObject());
        } catch (Exception e) {
            try {
                socket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        return null;
    }

    public void sendMessage(Message message) {
        synchronized (outputStream) {
            try {
                outputStream.writeObject(encryption.encryptDecrypt(message));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAuthorized() {
        return authorized;
    }

    public void setAuthorized(boolean authorized) {
        this.authorized = authorized;
    }

    public void keyExchange() {
        encryption.setPublicKeys(7, 251);
        encryption.setPersonalSecretKey(45);
        encryption.sendPublicKey();
        //member2.readPublicKey();
        encryption.sendEncodeSecretKey();
        //member2.readEncodeSecretCode();
        //member2.sendEncodeSecretKey();
        encryption.readEncodeSecretCode();
        encryption.findCommonSecretKey();
        //member2.findCommonSecretKey();
    }
    public boolean isConnected(){
        return socket.isConnected();
    }
    public void disconnect(){
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
