package diffieHellman;

import message.Message;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;


/**
 * Created by Руслан on 17.11.2016.
 */
public class Encryption {
    private int aPkey;
    private int nPkey;
    private int personalSecretKey;
    private int commonSecretKey;
    private int receivedEncodeSecretCode;
    private ObjectOutputStream outputStream;
    private ObjectInputStream inputStream;

    public Encryption(int secretKey) {
        this.personalSecretKey = secretKey;
    }

    public Encryption(ObjectOutputStream outputStream, ObjectInputStream inputStream) {
        this.outputStream = outputStream;
        this.inputStream = inputStream;
    }

    public void setPublicKeys(int aPkey, int nPkey){
        this.aPkey = aPkey;
        this.nPkey = nPkey;
    }
    public void setPersonalSecretKey(int personalSecretKey){
        this.personalSecretKey = personalSecretKey;
    }
    ///////////////////////////////////////////
    public void preset(){
        this.aPkey = 7;
        this.nPkey = 26;
        this.personalSecretKey = 11;
    }
    public void preset2(){
        this.personalSecretKey = 19;
    }

    public void preset3(){
        this.personalSecretKey = 16;
    }
    ///////////////////////////////////////////

    public void sendPublicKey(){
        if (aPkey==0||nPkey==0) {
            System.out.println("aPkey = "+aPkey+" nPkey = "+nPkey);
            return;
        }
        String data = ""+aPkey+"\n"+nPkey;
        sendMessage(data);

    }
    public void readPublicKey(){
        String[] data = new String[0];
        data = readMessage().split("\n");
        aPkey = Integer.parseInt(data[0]);
        nPkey = Integer.parseInt(data[1]);
    }
    public void sendEncodeSecretKey(){
        int result = new BigInteger(String.valueOf(aPkey)).modPow(new BigInteger(String.valueOf(personalSecretKey)), new BigInteger(String.valueOf(nPkey))).intValue();
        sendMessage(String.valueOf(result));
    }

    private String readMessage(){
        try {
//            Object message = null;
//            while (message==null){
//                message=inputStream.readObject();
//            }
//
            String received = ((Message)inputStream.readObject()).getData();
            return received;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Ошибка чтения сообщения");;
        }
        return "";
    }
    private void sendMessage(String data){
        try {
            outputStream.writeObject(new Message(data));
        } catch (IOException e) {
            System.out.println("Ошибка передачи соообщения");
        }
    }

    public synchronized Message encryptDecrypt(Message message) {
        Message s = null; try {
            s = message.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        String result = "";
        String data = s.getData();
        for (int i = 0; i < data.length(); i++) {
            result += (char)(data.charAt(i)^commonSecretKey);
        }
        s.setData(result);
        return s;
    }

    public void readEncodeSecretCode(){
        receivedEncodeSecretCode = Integer.parseInt(readMessage());
    }
    public int findCommonSecretKey(){
        commonSecretKey = new BigInteger(String.valueOf(receivedEncodeSecretCode)).modPow(new BigInteger(String.valueOf(personalSecretKey)), new BigInteger(String.valueOf(nPkey))).intValue();
        System.out.println(this.getClass().getSimpleName()+".findCommonSecretKey: " +commonSecretKey);
        return commonSecretKey;
    }

}
