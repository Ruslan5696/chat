package client;

import consolehelper.ConsoleHelper;
import diffieHellman.Encryption;
import message.Message;

import java.io.*;
import java.net.Socket;

/**
 * Created by Руслан on 03.12.2016.
 */
public class Client {
    protected Socket socket;
    protected ObjectOutputStream outputStream;
    protected ObjectInputStream inputStream;
    protected boolean connected;
    protected Encryption encryption;

    public static void main(String[] args) {
        Client client = new Client();
        //client.connect(client.serverAddressRequest(), client.portRequest());
        client.connect("localhost", client.portRequest());
    }

    protected class ReceiverThread extends Thread {

        @Override
        public void run() {
            while (!socket.isClosed()) {
                showMessage(receiveMessage());
            }
            System.out.println("Соединение с сервером потеряно");
            System.exit(0);
        }

        protected Message receiveMessage() {
            try {
                return encryption.encryptDecrypt((Message) inputStream.readObject());
            } catch (Exception e) {
                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            return new Message("Ошибка во время получения сообщения");
        }


        protected void showMessage(Message message) {
            System.out.println(message.getData());
        }
    }

    protected void connect(String ServerAddress, int port) {
        try {
            socket = new Socket(ServerAddress, port);
            outputStream = new ObjectOutputStream(socket.getOutputStream());
            inputStream = new ObjectInputStream(socket.getInputStream());
            encryption = new Encryption(outputStream, inputStream);
            connected = true;
            System.out.println("Соединение установлено");
            keyExchange();
            new ReceiverThread().start();
            sendingLoop();

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Соединение не установлено");

        }

    }

    public void keyExchange() {

        encryption.setPersonalSecretKey(17);
        encryption.readPublicKey();
        encryption.readEncodeSecretCode();
        encryption.sendEncodeSecretKey();
        encryption.findCommonSecretKey();

    }

    protected String serverAddressRequest() {
        System.out.println("Введите адрес сервера");
        return ConsoleHelper.readString();
    }

    protected int portRequest() {
        System.out.println("Введите порт сервера");
        return ConsoleHelper.readInt();
    }

    protected void sendingLoop() {
        String userMessage = "";
        while (!userMessage.equals("-exit")) {
            userMessage = ConsoleHelper.readString();
            sendMessage(userMessage);
        }
        connected = false;
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void sendMessage(String s) {
        try {
            outputStream.writeObject(encryption.encryptDecrypt(new Message(s)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
